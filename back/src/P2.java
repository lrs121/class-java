import java.util.*;

class P2 {

	public static int[] mklist(int len, int max, int min){
		Random r = new Random();
		int spread = max - min;
		int[] numbers = new int[len];
		for(int i = 0; i < len; i++) 
			numbers[i] = (r.nextInt() % (spread/2) + (spread/2)) + min;
		return numbers;
	}

	public static void main(String [] args) {
		int[] tl;
		long StartTime = System.nanoTime();
		tl = mklist(200, 100, 10);
		long EndTime = System.nanoTime();
		System.out.println("Time to create list: " + (EndTime - StartTime)/1000000.0 + " milliseconds");
	}
}
