
package javaclass;

import java.util.*;
import java.awt.*;

public class termveloc {

	static double termcalc(double m, double dr, double a){
		double vms;
		double vmh;
		double g = 9.8;
		double de = 1275.4;
		double presqrt;
		double grm = 453.592;
		presqrt = (2 * (m * grm) * g) / (de * a * dr);
		vms = Math.sqrt(presqrt);
		vmh = vms * 2.237;
		return vmh;
	}
	
	static void speedtime(double m, double dr, double a){
		double vms;
		double t;
		double g = 9.8;
		double de = 1275.4;
		double presqrt;
		double grm = 453.592;
		presqrt = (2 * (m * grm) * g) / (de * a * dr);
		vms = Math.sqrt(presqrt);
		t = vms * 2.237;
		double time = 0.0;
		double ts = 0.1;
		double v = 0.0;
		double dist = 0.0;
		while(v < (t * 0.99)){
			v = v + (g * ts *(1 - (v / t)));
			dist = dist + (v * ts);
			time = time + ts;
			System.out.println("Time: " + time + " Velocity: " + v + " Distance: " + dist);
		}
		
	}
	
	public static void main(String[] args) {
		double i;
		double j;
		double k;
		Scanner scan = new Scanner(System.in);
		System.out.println("Input object mass in lbs; ");
		i = scan.nextDouble();
		System.out.println("Input object Drag Coefficient ; ");
		j = scan.nextDouble();
		System.out.println("Input objet area in sq meters; ");
		k = scan.nextDouble();
		System.out.println("terminal velocity for object: " + termcalc(i, j, k) + " mph");
		speedtime(i, j, k);
	}

}
