
public class testrecursion {

	static long fib(int x){
		if(x == 1 || x == 2)
			return 1;
			return fib(x - 1) + fib(x - 2);
	}

	public static void main(String[] args) {
		long stime = System.nanoTime();
		fib(50);
		long etime = System.nanoTime();
		System.out.println("time to search 1000 numbers " + (etime - stime) / 1000000.0
				+ " milliseconds");
	}

}
