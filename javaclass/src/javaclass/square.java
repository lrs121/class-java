package javaclass;

import java.applet.Applet;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Graphics;
import java.awt.Color;

public class square extends Applet implements MouseListener {
	
	/*
	 * used to get things to work
	 */
	
	boolean clicked = false;
	boolean pressed = false;
	int x;
	int y;
	
	/*
	 * the init, what to load on applet start
	 */
	
	public void init() {
		addMouseListener(this);
	}
	
	/*
	 * Defines what the applet does
	 */
	
	public void paint(Graphics g) {
		//g.drawString("Hello world!", 50, 25);
		//g.drawString("Hello world", 50, 100);
		if(clicked){
			g.setColor(Color.yellow);
			g.drawRect(x, y, 50, 50);
		}
		if(pressed){
			g.fillRect(x, y, 50, 50);
			
		}
	}
		
	/*
	*Events required for mouse listener and mouse events to work
	*/
	
	public void mouseEntered(MouseEvent e){
		
	}
	public void mouseExited(MouseEvent e){
		clicked = false;
		repaint();
	}
	public void mouseClicked(MouseEvent e){
		x = e.getX();
		y = e.getY();
		clicked = true;
		repaint();
	}
	public void mousePressed(MouseEvent e){
		x = e.getX();
		y = e.getY();
		pressed = true;
		repaint();
	}
	public void mouseReleased(MouseEvent e){
		x = e.getX();
		y = e.getY();
		pressed = false;
		repaint();
	}
}
