import java.applet.Applet;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.Graphics;

public class AppletDemo extends Applet implements MouseListener {
	boolean clicked = false;
	int X = 0;
	int Y = 0;
	
	public void init() {
		addMouseListener(this);
	}

	public void paint(Graphics g) {
		  g.drawString("Hello world!", 50, 25);
		  g.drawString("Hello world!", 50, 100);
		  if(clicked)
			  g.drawRect(X, Y, 40, 40);
	 }

	 public void mouseEntered(MouseEvent e){

	 }
	 public void mouseExited(MouseEvent e){

	 }
	 public void mouseClicked(MouseEvent e){
		X = e.getX();
		Y = e.getY();
		clicked = true;	 	
		repaint();
	 }
	 public void mousePressed(MouseEvent e){

	 }
	 public void mouseReleased(MouseEvent e){

	 }

}

