package javaclass;

import java.io.*;
import java.util.Scanner;

public class runprogram {
	
	    public static void main(String[] args) {    	
	    	Scanner scan = new Scanner(System.in);
	    	
	    	try {
	    		Process p = Runtime.getRuntime().exec("ls -oa /home/lrs");//Windows command, use "ls -oa" for UNIX
	    		Scanner sc = new Scanner(p.getInputStream());    		
	    		while (sc.hasNext()) System.out.println(sc.nextLine());
	    	}
	    	catch (IOException e) {
	    		System.out.println(e.getMessage());
	    	}
	    }
}
