import java.util.Arrays;

class Quicksort {
	static void swap(int [] A, int x, int y){
		int temp = A[x];
		A[x] = A[y];
		A[y] = temp;

	}

	public static void bubblesort(int [] A, int start, int end){
		boolean changed = true;
		int len = end - start;
		while(changed){
			changed = false;
			for(int i = 0; i < (len - 1); i++)
				if(A[i+start] > A[i+1+start]){
					swap(A, i+start, i+1+start);
					changed = true;
				}
		}
	}

	public static void quicksort_recur(int [] A, int start, int end){
		int len = end - start;
		int middle = len/2 + start;
		// Calculate median value, store in A[middle]
		if(A[start] > A[end-1])
			swap(A, start, end-1);
		if(A[middle] < A[start])
			swap(A, middle, start);
		if(A[end-1] < A[middle])
			swap(A, middle, end-1);
		// Move median value to 2nd to last element 
		// The last element is greater than the pivot, for sure
		swap(A, middle,end-2);
	
		// Move everything to the right half of the array
		int i = start-1;
	 	int j = end-2;
		int pivot = A[end-2];
		while(true) {
			while(A[++i] < pivot); // Linear search for an element less than the pivot
			while(pivot < A[--j]); 
			if(i < j) 
				swap(A, i, j);
			else 
				break;
		}

		// Pivot is in the wrong spot - needs to be put back in place
		// Why is spot i the right place?  It's the first item that 
		// belongs in the second half.
		if(i < end-2)
			swap(A, i, end-2);
		
		// If less than 10 items, call bubblesort
		if(i-start > 10) 
			quicksort_recur(A, start, i);
		else
			bubblesort(A, start, i);
	
		if(end-i > 10)
			quicksort_recur(A, i+1,end); // i+1, don't sort the pivot, it's fine where it is.
		else
			bubblesort(A, i+1,end);
			
	}

	/*
	 * Use this one for the project.  Remember, it depends on quicksort, bubblesort, and swap, 
	 * so you'll have to have all of them.  All these methods are static, so you can leave them
	 * in the Quicksort class if you like and just call them from your class, or you can cut
	 * and paste them all in.  It'll be neater if you just call them from your class, using
	 * Quicksort.quicksort(myarray), etc.  
	 */
	public static void quicksort(int [] A){
		quicksort_recur(A, 0, A.length);
	}

	public static void printarray(int [] A){
		for(int i = 0; i < A.length; i++)
			System.out.print(A[i] + " ");
		System.out.println();
	}

	/*
	 * This method only exists to test the quicksort method.
	 */
	public static void main(String [] args){
		int [] testarray = {100, 5, 7, 3, 6, 8, 9, 3, 2, 45, 76, 7, 3, 2, 5, 6, 87, 3, 2, 5, 6, 7, 8, 5, 3, 2, 5, 6, 3, 2, 5, 76, 8, 8, 8, 53, 2, 2, 4, 25, 11, 21, 31,234};
		quicksort(testarray);
		printarray(testarray);

	}
}
