import java.util.Scanner;

public class sep30 {
	
	public static float repay(float y, float p){
		//the equation
		float py = p * 12;
		float a = y * py;
		//print
		return a;
	}
	public static float change(int pe, int ni, int di, int qu){
		//finding the total for each denomination 
		int tp = 1 * pe;
		int tn = 5 * ni;
		int td = 10 * di;
		int tq = 25 * qu;
		//add them all together
		float tc = tp + tn + td + tq;
		//convert to float
		float amnt = tc / 100;
		//print
		return amnt;
	}
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter years: ");
		float year = scan.nextInt();
		System.out.print("Enter Payment amount: ");
		float payment = scan.nextInt();
		System.out.println("Total repayment: " + repay(year , payment));
		System.out.print("Enter number of pennies: ");
		int pennies = scan.nextInt();
		System.out.print("Enter number of nickels: ");
		int nickels = scan.nextInt();
		System.out.print("Enter number of dimes: ");
		int dimes = scan.nextInt();
		System.out.print("Enter number of quarters: ");
		int quarters = scan.nextInt();
		System.out.println("Total Change: $" + change(pennies, nickels, dimes, quarters));
	}

}
