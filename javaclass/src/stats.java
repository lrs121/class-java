

import java.io.FileReader;
import java.io.BufferedReader;

class Word {
	public String word;
	public int spamcount;
	public int hamcount;

	public Word(String w) {
		word = w;
	}

	public double spamval(double tspam, double tham) {
		return spamcount / tspam - hamcount / tham;
	}
}

public class stats {
	// some list of words
	static Word words[] = new Word[10000];
	static int wnum = 0;
	static String spamwords[] = new String[10000];
	static String hamwords[] = new String[10000];
	static int swc = 0;
	static int hwc = 0;

	public static void process_messages(String file, boolean is_spam) {
		try {
			BufferedReader ourfile = new BufferedReader(new FileReader(file));
			while (ourfile.ready()) {
				String line = ourfile.readLine();
				String[] sp_line = line.split("\\s+");
				if (sp_line[0].contains(":"))
					continue;
				for (int i = 0; i < sp_line.length; i++) {
					if (sp_line[i].contains("@"))
						continue;
					if (is_spam) {
						spamwords[swc] = sp_line[i];
						swc++;
					} else {
						hamwords[hwc] = sp_line[i];
						hwc++;
					}
				}

			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	/*
	 * What this does: If the word is in the array "words", return the index it
	 * was found at Otherwise, return -1
	 */
	public static int findword(String w) {
		for (int i = 0; i < wnum; i++) {
			if (words[i].word.equals(w))
				return i;
		}
		return -1;
	}

	public static void printwords() {
		System.out.println("Number of words:  " + wnum);
		for (int i = 0; i < wnum; i++) {
			System.out.println(words[i].word + ": "
					+ words[i].spamval(spamwords.length, hamwords.length));
		}
	}

	public static boolean is_spam(String[] mw, int mwc) {
		/*
		 * running total of scores starts at 0.0 for each word in mw: 
		 * 		look up word in words if it's not there: 
		 * 			continue 
		 * 		add score to scores so far
		 * return true if score is positive 
		 * return false if score is negative or 0
		 */
		double score = 0.0;
		for (int i = 0; i < mwc; i++) {
			int indx = findword(mw[i]);
			if(indx == -1)
				continue;
			score += words[i].spamval(swc, hwc);
			
		}
		if(score > 0){
			return true;
		}
		return false;
	}

	public static boolean run_is_spam_on_file(String filename) {
		String our_words[] = new String[1000];
		int owc = 0;
		try {
			BufferedReader ourfile = new BufferedReader(
					new FileReader(filename));
			while (ourfile.ready()) {
				String line = ourfile.readLine();
				String[] sp_line = line.split("\\s+");
				if (sp_line[0].contains(":"))
					continue;
				for (int i = 0; i < sp_line.length; i++) {
					if (sp_line[i].contains("@"))
						continue;
					our_words[owc] = sp_line[i];
					owc++;
				}

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return is_spam(our_words, owc);
	}

	public static void main(String[] args) {
		process_messages(
				"/home/lrs/projects/class/java/messages/spam_messages", true);
		process_messages("/home/lrs/projects/class/java/messages/ham_messages",
				false);
		// go through spamwords
		for (int i = 0; i < swc; i++) {
			// if we've never seen spamword[i]
			// add it to the list of words
			// if it's already in the list
			// increment it's spam count
			int idx = findword(spamwords[i]);
			if (idx == -1) {
				words[wnum] = new Word(spamwords[i]);
				words[wnum].spamcount++;
				wnum++;
			} else {
				words[idx].spamcount++;
			}
		}
		for (int i = 0; i < hwc; i++) {
			int idx = findword(hamwords[i]);
			if (idx == -1) {
				words[wnum] = new Word(hamwords[i]);
				words[wnum].hamcount++;
				wnum++;
			} else {
				words[idx].hamcount++;
			}
		}
		printwords();
		if (run_is_spam_on_file("/home/lrs/projects/class/java/messages/test_spam_message"))
			System.out.println("We correctly identified a spam message!");
		else
			System.out.println("Oh no, we missed one!");

		if (run_is_spam_on_file("/home/lrs/projects/class/java/messages/test_ham_message"))
			System.out.println("You'll never get the joke your uncle sent you");
		else
			System.out.println("We correctly let it pass!");
	}

}
