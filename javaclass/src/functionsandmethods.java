import java.util.*;

public class functionsandmethods {

	static void print_line(int i) {
		while (i > 0) {
			System.out.print("-");
			i--;
		}
		System.out.println();

	}

	static double raise(double x) {
		return Math.pow(x, x);
	}

	static double shift(double i, int j) {
		double k = i * Math.pow(10, j);
		return k;
	}

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter how long the lenght of the bar shoud be: ");
		int i = scan.nextInt();
		print_line(i);
		System.out.println("Enter the number to be raised by itself: ");
		double j = scan.nextDouble();
		System.out.println(raise(j));
		System.out.println(shift(123.4, -2));
	}

}
