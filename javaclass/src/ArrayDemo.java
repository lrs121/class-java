
public class ArrayDemo {

	static void printarray(int[] A){
		int i = 0;
		while(i < A.length){
			System.out.print(A[i] + " ");
			i++;
		}
		System.out.println("");
	}
	
	public static void main(String[] args) {
		int A[] = {3, 1, 5, 6, 3, 17, 10, 4, 3, 3};
		printarray(A);
		
		float sum = 0;
		for(int i = 0; i < A.length; i++){
			sum += A[i];
		}
		System.out.println("Sum of array: " + sum);
		System.out.println("Average of array: " + (sum / A.length));
		System.out.println("Maximum of array: " + maximum(A));
		int B[] = {-10000, -3000, -40000, -6000};
		System.out.println("Maximum of B:" + maximum(B));
		System.out.println("Minimum of arrays (A, B): " + minimum(A) + " " + minimum(B));
		System.out.println("Median of A: " + median(A));
		System.out.println("Median of B: " + median(B));
		System.out.println("Number of threes: " + threes(A));
		System.out.println("Number of unique items:  " + unique_numbers(A));
		int C[] = new int[100];
		printarray(C);
	}
	
	static int minimum(int[] A){
		int min = A[0];
		for(int i = 1; i < A.length; i++){
			if(A[i] < min)
				min = A[i];
		}
		return min;
	}
	
	static int maximum(int[] A){
		int max = A[0];
		for(int i = 1; i < A.length; i++){
			if(A[i] > max)
				max = A[i];
		}
		return max;
	}
	
	static double median(int[] A){
		int temp;
		for(int j = 0; j < A.length - 1; j++){
			int midx = j;
			for(int i = j+1; i < A.length; i++){
				if(A[i] < A[midx])
					midx = i;
			}
			temp = A[midx];
			A[midx] = A[j];
			A[j] = temp;
		}
		printarray(A);
		if(A.length % 2 == 1)
			return A[A.length / 2];
		else
			return (A[A.length/2] + A[A.length/2 - 1]) / 2.0;
	}

	static int threes(int[] A){
		int count = 0;
		for(int i = 0; i < A.length; i++){
			if(3 == A[i])
				count++;
		}
		return count;
	}
	
	static int unique_numbers(int[] A){
		int uniques = 0;
		int seenbefore[] = new int[A.length];
		for(int i = 0; i < A.length; i++){
			// search seenbefore up to position uniques for item A[i]
			boolean foundit = false;
			for(int j = 0; j < uniques; j++){
				if(A[i] == seenbefore[j])
					foundit = true;
			}
			// if we haven't seen item A[i] before
			// if A[i] isn't in seenbefore up to position uniques
			if(!foundit){
				seenbefore[uniques] = A[i];				
				uniques++;				
			}
		}
		return uniques;
	}
}
