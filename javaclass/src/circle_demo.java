import java.awt.Color;
import java.lang.*;
public class circle_demo {

	public static void circle(Turtle T, int size){
		int i = 0;
		while(i < 360) {
			T.turn(1);
			T.forward(size);
			i++;
		}
	}
	public static void colorcircle(Turtle T, int size) {
		colorcircle(T, size, true);		
	}
	
	public static void colorcircle(Turtle T, int size, boolean slow) {
		int i = 0;
		while(i < 360) {
			T.turn(1);
			T.forward(size);
			T.setColor(new Color(128, (Math.abs(180-i)*255)/180, 128));
			i++;
			if(slow){
				try {
					Thread.sleep(1);
				} catch (Exception e) {
					System.out.println("Something went wrong!");
				}
			}
		}		
	}
	
	public static void main(String[] args) {
        World world = new World(1000,700);
        Turtle red = new Turtle(world);
        red.setColor(Color.red);
        red.forward(250);
        Turtle green = new Turtle(world);
        green.setColor(new Color(160,0,200));
        green.turn(90);
       // green.forward(100);
        circle(green, 4);
        red.turn(90);
        int i = 0;
        while(i < 360){
        	green.turn(5);
        	colorcircle(green,  3, false);
        	i = i + 5;
        }
        i=0;
        while(i < 100){
        	colorcircle(red, 6);
        	i++;
        	red.turn(1);
        }
        

	}

}
