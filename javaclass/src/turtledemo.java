import java.awt.Color;


public class turtledemo {
	public static void main(String[] args) {
        World world = new World();
        Turtle sam = new Turtle(world);
        int sidelength = 100;
        /*
        sam.forward(sidelength);
        sam.turn(120);
        sam.forward(sidelength);
        sam.turn(120);
        sam.forward(sidelength);
        sam.penUp();
        sam.forward(35);
        sam.turn(-60.0);
        sidelength = 300;
        sam.forward(25); // we would like no line
        sam.penDown();
        sam.turn(60);
        
        // Draw Triangle (Starts here)
        sam.turn(120);
        sam.forward(sidelength);
        sam.turn(120);
        sam.forward(sidelength);
        sam.turn(120);
        sam.forward(sidelength);
        // Draw Triangle (Ends here)
        */
        sam.turn(180);
        sam.setColor(Color.black);
        int i = 0;
        while(i < 50){
        	sidelength = i*10;
            // Draw Triangle (Starts here)
            sam.turn(120);
            sam.forward(sidelength);
            sam.turn(120);
            sam.forward(sidelength);
            sam.turn(120);
            sam.forward(sidelength);
            
            sam.turn(-30);
            sam.penUp();
            sam.forward(5);
            sam.penDown();
            sam.turn(30);
            //sam.turn((50-i)/25);
            // Draw Triangle (Ends here)        	
        	i = i + 1;
        	/*if(i % 2 == 0){
        		sam.setColor(Color.magenta);
        	}
        	else
        		sam.setColor(Color.green);
        }
        sam.setColor(Color.RED);*/
	}
	}
}
