
public class functions {

	public static double distance(double a, double b){
		double result = Math.sqrt(a*a + b*b);
		return result;
	}

	public static double screensize(double diagonal, double a, double b){
		// x represents the size of the monitor relative to a "unit monitor"
		// defined by the size of a monitor that is exactly the size of the
		// ratio.  That is, if the ratio is 16:10, the "unit monitor" is 
		// 16" by 10", and x is how much longer than that each side is.
		double x = Math.sqrt(Math.pow(diagonal, 2) / (a*a + b*b));
		a = a * x;
		b = b * x;
		return a * b;
	}
	
	public static long factorial(int x){
		long  acc = 1;
		int i = 1;
		while(i <= x){
			acc = acc * i;
			i++;
		}
		return acc;
	}
	
	public static int number_of_symbols(int number_of_pieces){
		return (int) Math.pow(2, number_of_pieces);
	}
	
	public static double f(double x){
		return x*x;
	}
	
	public static double integral(double lowlimit, double highlimit){
		double acc = 0;
		double i = lowlimit;
		while(i < highlimit){
			acc += f(i) * 0.001;
			i += 0.001;
		}
		return acc;
	}
	
	public static double rocketsled(double avgthrust, double ti, double weight){
		double thrust_lbs = avgthrust/4.45;
		double duration = ti / avgthrust;
		double acc_g = thrust_lbs / weight;
		if(acc_g > 3){
			System.out.println("Accelleration is " + acc_g + ", recommend visiting your chiropractor afterwards");
		}
		double acc_mph = acc_g * 21.937;
		return acc_mph * duration;
	}
	
	public static void main(String[] args) {
		System.out.println("Testing distance:  " + distance(1000, 1000));
		System.out.println("Size of a monitor:" + screensize(28, 16, 9));
		System.out.println("Factorial of something: " + factorial(25));
		System.out.println("With 20 character pieces: " + number_of_symbols(20));
		System.out.println("With 8 character pieces: " + number_of_symbols(8));
		System.out.println("Integral : " + integral(5, 10));
		System.out.println("Rocket Sled speed: " + rocketsled(4800, 7700*2, 185));
	}

}
