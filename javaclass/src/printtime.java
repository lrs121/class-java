
public class printtime {

	static void printarray(int[] A){
		int i = 0;
		while(i < A.length){
			System.out.print(A[i] + " ");
			i++;
		}
		System.out.println("");
	}
	
	static int maximum(int[] A){
		int max = A[0];
		//long istime = System.nanoTime();
		for(int i = 1; i < A.length; i++){
			if(A[i] > max)
				max = A[i];
		}
		//long ietime = System.nanoTime();
		return max;
	}
	
	public static void main(String[] args) {
		long tstime = System.nanoTime();
		long tetime = System.nanoTime();
		System.out.println("time to do nothing = " + (tetime - tstime)/1000000.0);
		int a[] = {1, 2, 3, 4, 5, 15, 7, 8, 9, 1};
		int b = 3;
		int c = 9;
		int d;
		long fstime = System.nanoTime();
		printarray(a);
		long fetime = System.nanoTime();
		System.out.println("time to print 10 = " + (fetime - fstime)/1000000.0);
		long sstime = System.nanoTime();
		System.out.println("Maximum of B:" + maximum(a));
		long setime = System.nanoTime();
		System.out.println("time to find max = " + (setime - sstime)/1000000.0);
		long fostime = System.nanoTime();
		d = c + b;
		long foetime = System.nanoTime();
		System.out.println(d);
		System.out.println("time to add = " + (foetime - fostime)/1000000.0);
	}


}
