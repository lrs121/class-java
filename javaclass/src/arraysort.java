
public class arraysort {

	static void printarray(int[] A){
		int i = 0;
		while(i < A.length){
			System.out.print(A[i] + " ");
			i++;
		}
		System.out.println("");
	}
	
	static void reverse(int[] a){
		for(int i = 0; i < a.length / 2; i++){
			int temp = a[i];
		    a[i] = a[a.length -1 - i];
		    a[a.length - 1 - i] = temp;
			
		}
	}
		
	public static void main(String[] args) {
		int a[] = {1, 2, 3, 4, 5, 6};
		printarray(a);
		reverse(a);
		printarray(a);
		other(a);
		printarray(a);
	}
	static void other(int [] a){
 		for(int left = 0, right = a.length -1; left < right; left++, right--){
 			 int temp = a[left];
 			 a[left] = a[right];
 			 a[right] = temp;
 		}
 	}
}
 	