import java.util.*;

public class thorpe_larson_p4 {

	public static int[] mklist(int len, int max, int min) {
		Random r = new Random();
		int spread = max - min;
		int[] numbers = new int[len];
		for (int i = 0; i < len; i++)
			numbers[i] = (r.nextInt() % (spread / 2) + (spread / 2)) + min;
		return numbers;
	}

	/*
	 * quick sort algorithm * int temp = A[x]; A[x] = A[y]; A[y] = temp;
	 * 
	 * }
	 * 
	 * public static void bubblesort(int[] A, int start, int end) { boolean
	 * changed = true; int len = end - start; while (changed) { changed = false;
	 * for (int i = 0; i < (len - 1); i++) if (A[i + start] > A[i + 1 + start])
	 * { swap(A, i + start, i + 1 + start); changed = true; } } }
	 * 
	 * public static void quicksort_recur(int[] A, int start, int end) { int len
	 * = end - start; int middle = len / 2 + start; // Calculate median value,
	 * store in A[middle] if (A[start] > A[end - 1]) swap(A, start, end - 1); if
	 * (A[middle] < A[start]) swap(A, middle, start); if (A[end - 1] <
	 * A[middle]) swap(A, middle, end - 1); // Move median value to 2nd to last
	 * element // The last element is greater than the pivot, for sure swap(A,
	 * middle, end - 2);
	 * 
	 * // Move everything to the right half of the array int i = start - 1; int
	 * j = end - 2; int pivot = A[end - 2]; while (true) { while (A[++i] <
	 * pivot) ; // Linear search for an element less than the pivot while (pivot
	 * < A[--j]) ; if (i < j) swap(A, i, j); else break; }
	 * 
	 * // Pivot is in the wrong spot - needs to be put back in place // Why is
	 * spot i the right place? It's the first item that // belongs in the second
	 * half. if (i < end - 2) swap(A, i, end - 2);
	 * 
	 * // If less than 10 items, call bubblesort if (i - start > 10)
	 * quicksort_recur(A, start, i); else bubblesort(A, start, i);
	 * 
	 * if (end - i > 10) quicksort_recur(A, i + 1, end); // i+1, don't sort the
	 * pivot, it's // fine where it is. else bubblesort(A, i + 1, end);
	 * 
	 * }
	 * 
	 * /* Use this one for the project. Remember, it depends on quicksort,
	 * bubblesort, and swap, so you'll have to have all of them. All these
	 * methods are static, so you can leave them in the Quicksort class if you
	 * like and just call them from your class, or you can cut and paste them
	 * all in. It'll be neater if you just call them from your class, using
	 * Quicksort.quicksort(myarray), etc.
	 * 
	 * public static void quicksort(int[] A) { quicksort_recur(A, 0, A.length);
	 * }
	 */

	public static int recursiveBinarySearch(int[] sortedArray, int start,
			int end, int key) {

		if (start < end) {
			int mid = start + (end - start) / 2;
			if (key < sortedArray[mid]) {
				return recursiveBinarySearch(sortedArray, start, mid, key);

			} else if (key > sortedArray[mid]) {
				return recursiveBinarySearch(sortedArray, mid + 1, end, key);

			} else {
				return mid;
			}
		}
		return -(start + 1);
	}

	public static void binarysearch(int [] A, int b){
		recursiveBinarySearch(A, 0, A.length, b);
	}
	
	public static void main(String[] args) {
		int[] tl = mklist(200, 100, 10);;
		int[] tl2 = mklist(1000, 1000, 10);
		int[] tl3 = mklist(10000, 10000, 10);
		int[] tl4 = mklist(100000, 100000,10);
		long longtime = System.nanoTime();
		Quicksort.quicksort(tl);
		long endtime = System.nanoTime();
		System.out.println("Time to quicksort list: " + (endtime - longtime)
				/ 1000000.0 + " milliseconds");
		long qstime = System.nanoTime();
		Quicksort.quicksort(tl2);
		long qetime = System.nanoTime();
		System.out.println("Time to quicksort2 list: " + (qetime - qstime)
				/ 1000000.0 + " milliseconds");
		long qsstime = System.nanoTime();
		Quicksort.quicksort(tl3);
		long qeetime = System.nanoTime();
		System.out.println("Time to quicksort3 list: " + (qeetime - qsstime)
				/ 1000000.0 + " milliseconds");
		long qssstime = System.nanoTime();
		Quicksort.quicksort(tl4);
		long qeeetime = System.nanoTime();
		System.out.println("Time to quicksort3 list: " + (qeeetime - qssstime)
				/ 1000000.0 + " milliseconds");
		long bstime = System.nanoTime();
		binarysearch(tl, 50);
		long betime = System.nanoTime();
		System.out.println("Time to binarysearch list: " + (betime - bstime)
				/ 1000000.0 + " milliseconds");
		long bsstime = System.nanoTime();
		binarysearch(tl2, 500);
		long beetime = System.nanoTime();
		System.out.println("Time to binarysearch2 list: " + (beetime - bsstime)
				/ 1000000.0 + " milliseconds");
		long bssstime = System.nanoTime();
		binarysearch(tl3, 5000);
		long beeetime = System.nanoTime();
		System.out.println("Time to binarysearch3 list: " + (beeetime - bssstime)
				/ 1000000.0 + " milliseconds");
		long bsssstime = System.nanoTime();
		binarysearch(tl4, 50000);
		long beeeetime = System.nanoTime();
		System.out.println("Time to binarysearch4 list: " + (beeeetime - bsssstime)
				/ 1000000.0 + " milliseconds");
		
	}

}
