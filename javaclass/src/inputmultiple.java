import java.util.Scanner;

public class inputmultiple {

	static void mtablescan(){
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter number to make a multiplication table for: ");
		int k = scan.nextInt();
		for(int i =1; i <= k; i++){
			System.out.print(i + ": ");
				for(int j = 1; j <= k; j++){
					System.out.print(i*j + " ");
				}
				System.out.println();
		}
	}
	
	static void mtable(int k){
		//Scanner scan = new Scanner(System.in);
		//System.out.println("Enter number to make a multiplication table for.: ");
		//int k = scan.nextInt();
		for(int i =1; i <= k; i++){
			System.out.print(i + ": ");
				for(int j = 1; j <= k; j++){
					System.out.print(i*j + " ");
				}
				System.out.println();
		}
	}
	
	public static void main(String[] args) {
		mtable(16);
		System.out.println();
		mtablescan();
	}

}
