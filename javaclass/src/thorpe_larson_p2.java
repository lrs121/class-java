import java.util.*;

public class thorpe_larson_p2 {

	public static int[] mklist(int len, int max, int min) {
		Random r = new Random();
		int spread = max - min;
		int[] numbers = new int[len];
		for (int i = 0; i < len; i++)
			numbers[i] = (r.nextInt() % (spread / 2) + (spread / 2)) + min;
		return numbers;
	}

	public static int search(int[] tl, int key) {

		for (int i = 0, size = tl.length; i < size; i++) {
			if (tl[i] == key) {
				return i;
			}
		}
		return 0;

	}

	public static void bubble(int[] tl) {
		for (int n = tl.length, k, m = n; m >= 0; m--) {
			for (int i = 0; i < n - 1; i++) {
				k = i + 1;
				if (tl[i] > tl[k]) {
					swap(i, k, tl);
				}
			}
		}
	}

	public static void swap(int i, int j, int[] tl) {
		int tmp;
		tmp = tl[i];
		tl[i] = tl[j];
		tl[j] = tmp;
	}

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		int[] tl1;
		long StartTime = System.nanoTime();
		tl1 = mklist(200, 100, 10);
		long EndTime = System.nanoTime();
		System.out.println("Time to create list: " + (EndTime - StartTime)
				/ 1000000.0 + " milliseconds");
		int key = 12;
		int[] tl2 = mklist(1000, 1, 1000);
		long stime = System.nanoTime();
		search(tl2, key);
		long etime = System.nanoTime();
		System.out.println("time to search 1000 numbers " + (etime - stime) / 1000000.0
				+ " milliseconds");
		int[] tl3 = mklist(10000, 1, 1000);
		long s2time = System.nanoTime();
		search(tl3, key);
		long e2time = System.nanoTime();
		System.out.println("time to search 10000 numbers " + (e2time - s2time)
				/ 1000000.0 + " milliseconds");
		int[] tl4 = mklist(100000, 1, 1000);
		long s3time = System.nanoTime();
		search(tl4, key);
		long e3time = System.nanoTime();
		System.out.println("time to search 100000 numbers " + (e3time - s3time)
				/ 1000000.0 + " milliseconds");
		long sttime = System.nanoTime();
		bubble(tl2);
		long ettime = System.nanoTime();
		System.out.println("time to sort 1000 numbers " + (ettime - sttime)
				/ 1000000.0 + " milliseconds");
		long st2time = System.nanoTime();
		bubble(tl3);
		long et2time = System.nanoTime();
		System.out.println("time to sort 10000 numbers " + (et2time - st2time)
				/ 1000000.0 + " milliseconds");
		long st3time = System.nanoTime();
		bubble(tl4);
		long et3time = System.nanoTime();
		System.out.println("time to sort 100000 numbers " + (et3time - st3time)
				/ 1000000.0 + " milliseconds");
		int[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		long sstime = System.nanoTime();
		bubble(a);
		long estime = System.nanoTime();
		System.out.println("time to sort a presorted array " + (estime - sstime)
				/ 1000000.0 + " milliseconds");
		long sitime = System.nanoTime();
		long eitime = System.nanoTime();
		System.out.println("idle time for reference" + (eitime - sitime)/1000000.0 + " milliseconds");
	}

}
